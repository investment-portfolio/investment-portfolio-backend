# CODE REVIEWER CHECKLIST

Reviewer, please be sure to look at the review app in each of the browsers listed bellow.

CODE REVIEWER, certifique-se de olhar todos os itens contidos neste checklist estejam de acordo com a implementação código solicitada.

## CODE INDENTATION AND FORMATTING

Para um código mais limpo e legível, use a indentação de código com TAB.

Isso pode ser feito automaticamente pela IDE ou editor de textos com plugins. Por exemplo, use **CTRL+SHIFT+F** no Eclipse. Da mesma forma, **CTRL+ALT+L** no IntelliJ.

Faça uso das regras de formação Java! [Clique aqui](http://cr.openjdk.java.net/~alundblad/styleguide/index-v6.html#toc-formatting).

## OPTIMIZE IMPORTS

Sempre otimize os imports nas classes Java.